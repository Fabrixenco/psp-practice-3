import java.io.*;
import java.util.HashMap;

public class Translator {
    public static void main(String[] args) {
        //a condiction that check if recive parameters
        if (args.length > 0) {
            //get the rute for reed this parameter
            File file = new File(args[0]);

            //create a hasmap
            HashMap MyHashMap = new HashMap();

            try {
                //prepare the file for reed
                BufferedReader fileReader = new BufferedReader(new FileReader(file));
                String line1;

                BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
                String line;

                //a loop for reed file and put the word in the hasmap
                while ((line1 = fileReader.readLine()) != null) {
                    String[] coincidencias = line1.split("-");
                    MyHashMap.put(coincidencias[0], coincidencias[1]);
                }

                //other loop for reed the commentLine and compare this words with hasmap
                while ((line = bufReadCommLine.readLine()) != null && !line.equalsIgnoreCase("Stop")) {

                    //if the  key of hasmap is equal for the line that recive print value key
                    if (MyHashMap.containsKey(line)) {
                        System.out.println(MyHashMap.get(line));
                    } else {
                        System.out.println("Unknown");
                    }
                }

                fileReader.close();
                bufReadCommLine.close();

            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        } else {

            HashMap MyHashMap = new HashMap();
            MyHashMap.put("Table", "Mesa");
            MyHashMap.put("Mirror", "Espejo");
            MyHashMap.put("Mouse", "Raton");
            BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
            String line;
            try {
                while ((line = bufReadCommLine.readLine()) != null && !line.equalsIgnoreCase("Stop")) {

                    if (MyHashMap.containsKey(line)) {
                        System.out.println(MyHashMap.get(line));
                    } else {
                        System.out.println("Unknown");
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }


    }


}
