import java.io.*;

public class PracticeEx4 {
    public static void main(String[] args) throws IOException {
        final String practice4Path = "out/artifacts/Translator_jar/Translator.jar";


        BufferedReader bufReader = null;
        BufferedWriter bufWriter = null;
        Process childProcess;

        try {

            childProcess = new ProcessBuilder("java", "-jar", practice4Path).start();

            OutputStream outStream = childProcess.getOutputStream();
            OutputStreamWriter outStrWriter = new OutputStreamWriter(outStream);
            bufWriter = new BufferedWriter(outStrWriter);

            //For read de ouput chill
            InputStream inStream = childProcess.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inStream);
            bufReader = new BufferedReader(inputStreamReader);

            //Read the command line
            BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));


            String word;
            do {
                //mesage for user
                System.out.println("Write a word for traducing in Spain ('Stop' to finish)");

                // word that send to the child
                word = bufReadCommLine.readLine();
                bufWriter.write(word);
                bufWriter.newLine();
                bufWriter.flush();

                //is the line where print the answer the child
                System.out.println(word + " the traduction is " + bufReader.readLine());

            } while (!word.equalsIgnoreCase("Stop"));


            //all the exception that can happen
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println("General Exception " + ex.getMessage());
        } finally {
            if (bufWriter != null) {
                bufWriter.close();
            }
            if (bufReader != null) {
                bufReader.close();
            }

        }


    }
}
