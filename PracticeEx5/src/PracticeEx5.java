import java.io.*;

public class PracticeEx5 {
    public static void main(String[] args) {
        //path the jar
        final String practice4Path = "out/artifacts/Adder_jar/Adder.jar";
        BufferedReader bufReader = null;


        Process childProcess;

        InputStream inStream;
        InputStreamReader inputStreamReader;

        String line;
        String result = "";
        int total = 0;
        //prepare the file for reed
        BufferedReader fileReader;

        //a condiction that check if recive parameters
        if (args.length > 0) {
            try {

                //loop for obtein the rutes
                for (String f : args) {


                    //create child depending on how many it contains args
                    childProcess = new ProcessBuilder("java", "-jar", practice4Path, f).start();
                    //childProcess.waitFor();


                    //For read de ouput chill
                    inStream = childProcess.getInputStream();
                    inputStreamReader = new InputStreamReader(inStream);
                    bufReader = new BufferedReader(inputStreamReader);


                    while ((line = bufReader.readLine()) != null) {

                        result = line;
                        if (line.contains("is not number")) {
                            System.out.println(line);
                        }

                    }
                    System.out.println("result of file " + f + " = " + result);
                    totalWrite("Total: " + result);
                    //acumulated and parse to int the result
                    total += Integer.parseInt(result);
                    bufReader.close();

                }

            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            //show total of all
            System.out.println("Total of all: " + total);

            //call method for create Totals.txt
            totalWrite("Total of all: " + total);
        }
    }

    public static void totalWrite(String total) {
        FileWriter file = null;
        PrintWriter pw = null;
        try {
            //create file Totals.txt
            //append for dont overwrite the document
            file = new FileWriter("./Totals.txt", true);
            pw = new PrintWriter(file);
            pw.write(total);
            pw.println("");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //comprove that file is close correctly
                if (null != file)
                    file.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}