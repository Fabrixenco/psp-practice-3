import java.io.*;

public class Adder {
    public static void main(String[] args) {
        //a condiction that check if recive parameters
        if (args.length > 0) {
            //get the rute for reed this parameter
            File file = new File(args[0]);
            System.out.println("Entraa");

            try {
                //prepare the file for reed
                BufferedReader fileReader = new BufferedReader(new FileReader(file));

                String line;
                String total;
                int numLine = 1;
                int suma = 0;
                while ((line = fileReader.readLine()) != null) {

                    //call the function that checks that it is number
                    if (isInteger(line)) {
                        suma += Integer.parseInt(line);

                    } else {
                        //Info for error in file and line
                        System.out.println("Error to file " + file.getName() + " in line " + numLine + " is not number");

                    }
                    //show the number the error line
                    numLine++;
                }
                System.out.println(suma);

                fileReader.close();


            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    //method that check if is number and return true or false
    public static boolean isInteger(String num) {

        if (num == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
